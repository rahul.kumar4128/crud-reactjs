import form from "@bit/sixsprints.core.beans-web/dist/forms/form";
import { Box, Button, FormControl, FormLabel, Input } from "@chakra-ui/react";
import React from "react";

const FormComponent = (props) => {
  const { onSubmit, register, edit, as } = props;

  return (
    <FormControl
      as={"form"}
      onSubmit={onSubmit}
      noValidate="noValidate"
      maxWidth='20%'
      margin='auto'
      isRequired
    >
      <FormLabel>Name:</FormLabel>
      <Input name="name" ref={register}></Input>
      <FormLabel>Address:</FormLabel>
      <Input name="address" ref={register}></Input>
      <FormLabel>mobile:</FormLabel>
      <Input name="mobile" ref={register}></Input>
      <Box>
        <Button type="submit">{edit ? "submit" : "Add"}</Button>
      </Box>
    </FormControl>
  );
};

export default FormComponent;

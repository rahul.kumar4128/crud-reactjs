import { Box, Flex, Image } from "@chakra-ui/react";
import React from "react";
import Nav from "../components/nav";
import LoginHero from "../icons/login-hero.svg";
import Form from "../router/login-form";
import {navigate}  from "@reach/router";
import {
  useFormController,
  restApi,
  storage,
} from "@bit/sixsprints.core.beans-web";

const onSuccess = (data, props) => {
  storage.set("userId", data.id);
  navigate('/login')
};

const SignUp = () => {
  const { register, onSubmit, watch } = useFormController({
    apiOptions: { url: "http://localhost:8080/posts", method: "post" },
    onSuccess,
  });

  console.log("watch()", watch());
  return (
    <Flex flexDirection="column" h="100vh">
      <Box>
        <Nav />
      </Box>
      <Flex flex={1}>
        <Box w="50%">
          <Image src={LoginHero} fit="cover" height="550px" pt={5} m="auto" />
        </Box>
        <Box w="50%" alignSelf="center">
          <Form
            register={register}
            onSubmit={onSubmit}
            heading="SignUP"
            btnLabel="Signup"
            linkTitle="Already have an account,Click here to login"
            path="/"
          />
        </Box>
      </Flex>
    </Flex>
  );
};

export default SignUp;

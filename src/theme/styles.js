
const styles = {
  global: {
    heading: {
      fontFamily: 'Raleway',
    },
    body: {
      fontFamily: 'Open Sans',
      color: 'white',
      bg: "#F3F4F6"
    },
  },
};

export default styles;

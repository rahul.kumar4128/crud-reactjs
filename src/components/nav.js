import { Avatar, Flex, ListItem, Spacer, UnorderedList } from '@chakra-ui/react';
import React from 'react';
import { Link, Router} from "@reach/router";

const Nav = () => {
    return ( 
        <Flex as={"nav"} borderRadius="5px" w="100%" bg="#6366F1">
        <Link to="/">
          <Avatar
            width="15%"
            ml={8}
            borderRadius="100%"
            name="Rahul Kumar"
            src="https://bit.ly/dan-abramov"
          />
        </Link>
        <Spacer></Spacer>
        <UnorderedList p={4} m={0} mr={8} width="15%" styleType="none">
          <Flex justifyContent="space-around">
            {/* <ListItem mr={3}>
              <Link to="/dashboard">Home</Link>
            </ListItem> */}
            <ListItem>
              {" "}
              <Link to="/">Login/SignUp</Link>{" "}
            </ListItem>
          </Flex>
        </UnorderedList>
      </Flex>
     );
}
 
export default Nav;
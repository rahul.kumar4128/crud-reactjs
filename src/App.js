import { ChakraProvider } from "@chakra-ui/react";
import React from "react";
import Home from "./home.js/home";
import theme from "./theme/theme";


function App() {
  return (
    <ChakraProvider theme={theme}>
      <Home />
    </ChakraProvider>
  );
}

export default App;

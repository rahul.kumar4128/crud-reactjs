import React, { useContext } from "react";
import { Router } from "@reach/router";
import Login from "../login/login";
import WelcomeScreen from "./welcome-screen";
import SignUp from "../sign-up/sign-up";
import { AuthProvider } from "../routes/auth";
import PublicRoutes from "../public.routes";
import PrivateRoute from "../private.routes";
import { Box } from "@chakra-ui/react";

const Home = () => {
  return (
    <Box>
      <AuthProvider>
        <PublicRoutes component={Login} path="/" />
        <PublicRoutes component={SignUp} path="/sign-up" />
        <PrivateRoute component={WelcomeScreen} path={"/dashboard"} />
      </AuthProvider>
    </Box>
  );
};

export default Home;

import {
  Box,
  Button,
  Center,
  Flex,
  Heading,
  Image,
  Text,
} from "@chakra-ui/react";
import React, { useContext } from "react";
import Nav from "../components/nav";
import Hero from "../icons/hero.svg";
import { TokenContext } from "../routes/auth";

const WelcomeScreen = () => {
  const { logout } = useContext(TokenContext);

  return (
    <Flex flexDirection="column" h="100vh">
      {/* <Nav /> */}
      <Flex flex={1} alignItems="center">
        <Box w="50%" flex={1}>
          <Heading
            as="h1"
            fontWeight="bold"
            maxWidth="45%"
            m="auto"
            color="#3B82F6"
          >
            Hello,I'm Rahul Kumar
          </Heading>
          <Text size="3xl" maxWidth="45%" m="auto" color="black">
            {" "}
            React Front End Devloper.Who Believe Customer satisfaction and user
            experience should be high priority.And my objective to build and
            application that satisfy Customer needs using latest Technology{" "}
          </Text>
          <Center>
            <Button onClick={logout} background="blue">
              logout
            </Button>
          </Center>
        </Box>
        <Box w="50%">
          <Image src={Hero} fit="contain"></Image>
        </Box>
      </Flex>
    </Flex>
  );
};

export default WelcomeScreen;

import React, { useEffect, useState } from "react";
import { storage } from "@bit/sixsprints.core.beans-web";

const TokenContext = React.createContext();

const AuthProvider = (props) => {
  const [token, setToken] = useState(storage.get('token'));
  // useEffect(() => {
  //   setToken(storage.get("token"));
  // }, [token]);
  const login = () => {
    setTimeout(() => {
      setToken(storage.get("token"));
    }, 100);
  };
  const logout = () => {
    storage.clearAll();
    setToken(null);
  };

  const logic = {
    token: token,
    login: login,
    logout: logout,
  };

  return (
    <TokenContext.Provider value={logic}>
      {props.children}
    </TokenContext.Provider>
  );
};
export { AuthProvider, TokenContext };

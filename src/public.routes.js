import React, { useContext } from "react";
import { Redirect, Router } from "@reach/router";
import Login from "./login/login";
import SignUP from "./sign-up/sign-up";
import { TokenContext } from "./routes/auth";
import WelcomeScreen from "./home.js/welcome-screen";

const PublicRoutes = (props) => {
  const { component: Component, path } = props;
  const { token } = useContext(TokenContext);
  return (
    <Router>
      {token ? <Redirect from={'*'} to="/dashboard" noThrow /> : <Component path={path} />}
    </Router>
  );
};

export default PublicRoutes;

import { useEffect, useState } from "react";
import { restApi, useFormController } from "@bit/sixsprints.core.beans-web";
import "./App.css";
import theme from "./theme/theme";

import FormComponent from "./form/form";
import Bro from "brototype";
import { ChakraProvider, Table, Tbody, Td, Tr } from "@chakra-ui/react";

function App() {
  const [list, setList] = useState([]);
  const [index, setIndex] = useState();
  const [edit, setEdit] = useState(false);
  const onSuccess = (data) => {
    
    setList((prev) => [...prev, data]);
    // restApi
    //   .fetch({ url: "http://localhost:8080/posts", method: "get" })
    //   .then((resp) => {
    //     let response = Bro(resp).iCanHaz("data");
    //     setList(response);
    //   });
    setEdit(false);
          setIndex(undefined);
    reset();
  };
  useEffect(()=>{

    console.log('hello');
  },

    [list]
  );

  // const { register, handleSubmit, watch, setValue, reset } = useForm();
  const { register, onSubmit, watch, setValue, reset } = useFormController({
    onSuccess,
    apiOptions: edit
      ? {
          url: `http://localhost:8080/posts/${index}`,
          method: "put",
        }
      : { url: "http://localhost:8080/posts", method: "post" },
  });

  // const addBtnClick = (data) => {
  //   if (edit) {
  //     onEdit(data);
  //     return;
  //   }

  //   restApi
  //     .fetch({ url: "http://localhost:8080/posts", data, method: "post" })
  //     .then((resp) => {
  //       let response = Bro(resp).iCanHaz("data");
  //       setList((prev) => [...prev, response]);
  //     });
  //   reset();
  // };
  // useEffect(() => {
  //   reset()
  //   console.log('list',list)
  // }, [list]);

  useEffect(() => {
    restApi
      .fetch({ url: "http://localhost:8080/posts", method: "get" })
      .then((resp) => {
        let response = Bro(resp).iCanHaz("data");
        setList(response);
      });
  }, []);
  console.log("watch()", watch());

  //delete an index
  const onDeleteClick = (ItemId) => {
    restApi
      .fetch({
        url: `http://localhost:8080/posts/${ItemId}`,
        method: "delete",
      })
      .then(() => {
        alert("sucessfull deleted");
        restApi
          .fetch({ url: "http://localhost:8080/posts", method: "get" })
          .then((resp) => {
            let response = Bro(resp).iCanHaz("data");
            setList(response);
          });
      });
  };

  //for updating
  const onUpdateClick = (item) => {
    console.log("item()", item);
    setValue("name", item.name);
    setValue("address", item.address);
    setValue("mobile", item.mobile);
    setEdit(true);
    setIndex(item.id);
  };

  // const onEdit = (data) => {
  //   console.log("Edit", data);
  //   restApi
  //     .fetch({
  //       url: `http://localhost:8080/posts/${index}`,
  //       data,
  //       method: "put",
  //     })
  //     .then((resp) => {
  //       alert("file updated sucessfully");
  //       restApi
  //         .fetch({ url: "http://localhost:8080/posts", method: "get" })
  //         .then((resp) => {
  //           let response = Bro(resp).iCanHaz("data");
  //           setList(response);
  //         });
  //       console.log("list after editing", list);
  //       setEdit(false);
  //       setIndex(undefined);
  //       reset();
  //     });
  // };

  const displayingList = () => {
    return (
      <ChakraProvider theme={theme}>
        <Table>
          <Tbody>
            {list.map((item, index) => (
              <Tr key={index}>
                <Td className="listTable">{item.name}</Td>
                <Td className="listTable">{item.address}</Td>
                <Td className="listTable">{item.mobile}</Td>
                <Td>
                  <button onClick={() => onDeleteClick(item.id)}>delete</button>
                  <button onClick={() => onUpdateClick(item, index)}>
                    edit
                  </button>
                </Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </ChakraProvider>
    );
  };

  return (
    <>
      <FormComponent
        // handleSubmit={handleSubmit}
        register={register}
        onSubmit={onSubmit}
        edit={edit}
        // onEdit={onEdit}
      />
      {list.length ? displayingList() : <div>no data</div>}
    </>
  );
}

export default App;

import {
  Box,
  Button,
  Center,
  Flex,
  FormLabel,
  Heading,
  Input,
} from "@chakra-ui/react";
import React from "react";
import { Link } from "@reach/router";

const Form = (props) => {
  const { register, onSubmit, heading, linkTitle, btnLabel, path } = props;

  return (
    <Center
      justifyContent="center"
      color="black"
      alignContent="center"
      alignItems={"center"}
    >
      <Box as={"form"} onSubmit={onSubmit}>
        <Heading as={"h1"} textAlign="center">
          {heading}
        </Heading>
        <Flex alignItems="center" flexDirection="column">
          <hr style={{ border: "1px solid #374151" }} width="50%"></hr>
          <FormLabel>Email</FormLabel>
          <Input
            mt="5px"
            type="text"
            name="email"
            ref={register}
            borderColor="black"
            isRequired
          />
          <FormLabel mt="1em">Password</FormLabel>
          <Input
            mb="5px"
            type="password"
            name="password"
            ref={register}
            borderColor="black"
            isRequired

          />
          <Link to={path} style={{color:'#2563EB'}}>{linkTitle}</Link>
          <Button type="submit" mt="1em" color="white" colorScheme="blue">
            {btnLabel}
          </Button>
        </Flex>
      </Box>
    </Center>
  );
};

export default Form;

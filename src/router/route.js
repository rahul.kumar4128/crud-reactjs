import React, { useState } from "react";
import { Link, Router } from "@reach/router";
import { Flex, Box, Heading } from "@chakra-ui/react";
import Form from "./login-form";
import {
  useFormController,
  restApi,
  storage,
} from "@bit/sixsprints.core.beans-web";

//Welcome Screen
const Welcome = () => {
  return (
    <Heading as="h1" size="4em">
      Hello,you have sucessfully Login
    </Heading>
  );
};

const SignUp = (props) => {
  const { signUpLabel } = props;
  const Success = (data) => {
    console.log("dataOFSignUp", data);
    storage.clearAll();
    storage.set("signUpId", data.id);
    console.log(storage.get("userId"));
  };

  const { register, onSubmit, watch } = useFormController({
    onSuccess: Success,
    apiOptions: { url: "http://localhost:8080/users", method: "post" },
  });

  console.log("watchingInputOFSignUp", watch());
  return (
    <>
      <Form register={register} onSubmit={onSubmit} signUpLabel={signUpLabel} />
    </>
  );
};

const LoginForm = () => {
  //onSucess
  const Success = (data) => {
    console.log("dataOFLogin", data);
    restApi
      .fetch({ url: `http://localhost:8080/users/`, method: "get" })
      .then((users) => {
        console.log(users.data);
        let userDataList=users.data
        // {userDataList.map()}
      });
  };
  //useOfUseFormController
  const { register, onSubmit, watch } = useFormController({
    onSuccess: Success,
  });

  console.log("watchingInputOfLogin", watch());
  return (
    <>
      <Form register={register} onSubmit={onSubmit} />
    </>
  );
};

const Demo = () => {
  const [signUpLabel] = useState(false);
  return (
    <>
      <Flex h="100vh">
        <Box border="1px solid red" w="30%">
          <Flex flexDirection="column" alignItems="center">
            <Link to="/">Sign Up</Link>
            <Link to="/login">Login</Link>
          </Flex>
        </Box>
        <Box border="1px solid green" w="70%">
          <Router>
            <SignUp path="/" signUpLabel={true} />
            <LoginForm path="/login" />
          </Router>
        </Box>
      </Flex>
    </>
  );
};

export default Demo;

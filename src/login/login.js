import { Box, Flex, Image } from "@chakra-ui/react";
import React, { useContext } from "react";
import Nav from "../components/nav";
import LoginHero from "../icons/login-hero.svg";
import Bro from "brototype";
import { TokenContext } from "../routes/auth";

import Form from "../router/login-form";
import {
  useFormController,
  restApi,
  storage,
} from "@bit/sixsprints.core.beans-web";
import { navigate } from "@reach/router";

const Login = () => {
  const { token, login } = useContext(TokenContext);
  const onSuccess = (data) => {
    storage.set("token", data.token);
    login();
  };

  const { register, onSubmit } = useFormController({
    onSuccess,
    apiOptions: { url: "auth/user/login", method: "post" },
  });

  return (
    <Flex flexDirection="column" h="100vh">
      <Box>
        <Nav />
      </Box>
      <Flex flex={1}>
        <Box w="50%">
          <Image src={LoginHero} fit="cover" height="550px" pt={5} m="auto" />
        </Box>
        <Box w="50%" alignSelf="center">
          <Form
            register={register}
            onSubmit={onSubmit}
            heading="Login"
            btnLabel="Login"
            linkTitle="Don't have an account?Create One."
            path="/sign-up"
          />
        </Box>
      </Flex>
    </Flex>
  );
};

export default Login;

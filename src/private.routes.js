import React, { useContext } from "react";
import { navigate, Redirect, Router } from "@reach/router";
import WelcomeScreen from "./home.js/welcome-screen";
import { TokenContext } from "../src/routes/auth";
import Login from "./login/login";
import SignUp from "./sign-up/sign-up";

const PrivateRoute = (props) => {
  const { component: Component, path } = props;
  const { token } = useContext(TokenContext);
  return (
    <Router>
      {token ?  <Component path={path} />: <Redirect from={'*'} to="/" noThrow /> }
    </Router>
  );
};

export default PrivateRoute;
